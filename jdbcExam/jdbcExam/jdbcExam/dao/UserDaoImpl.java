package jdbcExam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import jdbcExam.pojo.User;
import utilityExam.DBUtility;

public class UserDaoImpl implements UserDAO {
	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;
	ResultSet rs = null;
	
	@Override
	public void userLogin(User refUser) {
		
		try {
			refConnection = DBUtility.getConnection();
			// Statement stmt = refConnection.createStatement();

			// Getting the record in sql
			String sqlQuery = "SELECT user_id,user_password FROM user WHERE user_id=? and user_password=?";
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refUser.getUserID());
			refPreparedStatement.setString(2, refUser.getUserPassword());

			// execute the record
			rs = refPreparedStatement.executeQuery();
			boolean counter = false;
			int i = 0;

			// check if any records in table matches user input
			while (rs.next()) {
				String user_id = rs.getString("user_id");
				String user_password = rs.getString("user_password");

				if (refUser.getUserID().equals(user_id) && refUser.getUserPassword().equals(user_password)) {
					System.out.println("User Authenticated");
					break;

				} else {
					i++;
				}
				
				if(i==3) {
					System.out.println("Invalid Credentials");
				}
				
			}

		} catch (Exception e) {
			System.out.println("Exception Handled while getting record.");
		} finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
		
	}



	

}
