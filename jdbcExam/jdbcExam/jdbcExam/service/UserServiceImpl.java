package jdbcExam.service;
import java.util.Scanner;
import jdbcExam.dao.UserDAO;
import jdbcExam.dao.UserDaoImpl;
import jdbcExam.pojo.User;

public class UserServiceImpl implements UserService {

	UserDAO refUserDAO;
	Scanner scannerRef;
	User refUser;
	@Override
	public void checkInput() {
		// TODO Auto-generated method stub
		scannerRef = new Scanner(System.in);
		
		//request for user's input
		System.out.println("Enter user ID:");
		String userID = scannerRef.next();
		System.out.println("Enter user password");
		String password = scannerRef.next();
		
		// set and get the userId and password
		refUser = new User();
		refUser.setUserID(userID);
		refUser.setUserPassword(password);
		
		// call the userDao object
		refUserDAO = new UserDaoImpl();
		refUserDAO.userLogin(refUser);
		
	}

	
}
